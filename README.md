# JavaScript Funcional

Anotações e exercícios do curso de JavaScript Funcional e Reativo

## Paradigma

Modelo ou padrão a seguir.

JavaScript é uma linguagem multiparadigma. Pode-se escrever código procedural, OO ou funcional.

## Imperativo vs Declarativo

| Imperativo          | Declarativo          |
|---------------------|----------------------|
| Foco no fluxo       | Foco na logica       |
| Mutável             | Imutável             |
| Como                | O que                |
| Código mais extenso | Código menos extenso |
| Menos escalável     | Mais escalável       |
| Mais explícito      | Menos explícito      |

## Paradigma Funcional

- Funções são valores
- Imutabilidade (valor vs referência)
- Não possui acoplamento temporal
- Poucos problemas de concorrência

## Como funciona o JavaScript?

<img alt="Como funciona o JavaScript - " src="./assets/js-event-loop-47deg.png">

Fonte: [47deg](https://www.47deg.com/blog/road-to-frp-js-part-1/)
