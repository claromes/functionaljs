// Function declaration
function hello() {
  console.log('Hello')
}

hello()

// Function expression
const hi = function () {
  console.log('hi')
}

hi()