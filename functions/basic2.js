const fn1 = function () {
  console.log('This is fn1')
}

// function as parameter
const fn2 = function (fn) {
  if(typeof fn === 'function') {
    fn()
  }
}

fn2(3)
fn2(fn1)

// function return function
const pow = function (base) {
  return function (exp) {
    return Math.pow(base, exp)
  }
}

// use
const base = pow(2)
const pow8 = base(8)

console.log(pow8)

// or
const pow16 = pow(2)(16)

console.log(pow16)
