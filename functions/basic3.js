// arrow function

const pow = base => exp => Math.pow(base, exp)

const base = pow(2)
const pow8 = base(8)
console.log(pow8)

const sub = (a, b) => a - b
console.log(sub(5, 6))

// rest parameter

const sum = (...array) => {
  let total = 0
  for (let i of array) {
    total += i
  }
  return total
}

console.log(sum(1, 2, 3))