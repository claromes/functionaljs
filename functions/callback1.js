const sum = (a, b) => console.log(a + b)
const sub = (a, b) => console.log(a - b)

// callback functions
const exec = (fn, a, b) => fn(a, b)

exec(sum, 56, 38)
exec(sub, 182, 27)

const callback = () => console.log('Callback!')
setTimeout(callback, 2000)

setTimeout(() => console.log('Callback 2!'), 5000)