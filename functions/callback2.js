const fs = require('fs')
const path = require('path')

const filePath = path.join(__dirname, '..', 'assets', 'callback_data.txt')
const content = (_, file) => console.log(file.toString())  // _ for params not used

// content is the callback function. After read the file, content is called!
fs.readFile(filePath, content)

// without const
fs.readFile(filePath, (_, file) => console.log(file.toString()))