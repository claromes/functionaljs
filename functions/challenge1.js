// 1

const sum = function (num1) {
  return function (num2) {
    return function (num3) {
      return num1 + num2 + num3
    }
  }
}

console.log(sum(1)(50)(1))

// 2

const sumFn = function (sum1, sum2) {
  return sum1 + sum2
}

const subFn = function (sum1, sum2) {
  return sum1 - sum2
}

const calcFinal = function (num1) {
  return function (num2) {
    return function (Fn) {
      return Fn(num1, num2)
    } 
  }
}

const calc1 = calcFinal(20)(10)(sumFn)
const calc2 = calcFinal(20)(10)(subFn)


console.log(calc1)
console.log(calc2)