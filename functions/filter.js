// filter
const cart = [
  {product: 'pen', quantity: 2, price: 3.99},
  {product: 'pencil', quantity: 10, price: 0.99},
  {product: 'note', quantity: 8, price: 22.00},
  {product: 'eraser', quantity: 9, price: 2.59},
  {product: 'pen kit', quantity: 0, price: 16.00},
]

const getName = item => item.product
const hasQuanity = item => item.quantity > 0
const validCart = cart
  .filter(hasQuanity)
  .map(getName)

console.log(validCart)