// map
const grade = [7.5, 7.2, 8.9, 2.6]
const average = (n, i, arr) => (n + i)/arr.length

const final = grade.map(average)
console.log(final)

const names = ['bia', 'gui', 'rafa']
const firstL = n => n[0]

const result = names.map(firstL)
console.log(result)

const cart = [
  {product: 'pen', quantity: 2, price: 3.99},
  {product: 'pencil', quantity: 10, price: 0.99},
  {product: 'note', quantity: 8, price: 22.00},
  {product: 'eraser', quantity: 9, price: 2.59},
  {product: 'pen kit', quantity: 0, price: 16.00},
]

const getProduct = item => item.product
const getTotalPrice = item => item.quantity * item.price

const productNames = cart.map(getProduct)
const totalPrice = cart.map(getTotalPrice)

console.log(productNames)
console.log(totalPrice)

// build a map function

Array.prototype.myMap = function (fn) {
  const mapped = []
  for (let i = 0; i < this.length; i ++) {
    const result = fn(this[i], i, this)
    mapped.push(result)
  }

  return mapped
}

const getDoubleQuantuty = item => item.quantity * 2
const totalDoubleQuantuty = cart.myMap(getDoubleQuantuty)

console.log(totalDoubleQuantuty)